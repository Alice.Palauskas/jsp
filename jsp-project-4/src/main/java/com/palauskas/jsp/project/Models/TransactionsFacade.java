package com.palauskas.jsp.project.Models;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import java.util.List;
import java.util.Date;

@Stateless
public class TransactionsFacade {

    @PersistenceContext
    private EntityManager entityManager;

    public void createTransaction(Transactions transaction) {
        entityManager.persist(transaction);
    }

    public void updateTransaction(Transactions transaction) {
        entityManager.merge(transaction);
    }

    public void removeTransaction(Integer transactionID) {
        Transactions transaction = findTransactionById(transactionID);
        if (transaction != null) {
            entityManager.remove(transaction);
        }
    }

    public Transactions findTransactionById(Integer transactionID) {
        return entityManager.find(Transactions.class, transactionID);
    }

    public List<Transactions> findAllTransactions() {
        TypedQuery<Transactions> query = entityManager.createNamedQuery("Transactions.findAll", Transactions.class);
        return query.getResultList();
    }

    public List<Transactions> findTransactionsByCheckoutDate(Date checkoutDate) {
        TypedQuery<Transactions> query = entityManager.createNamedQuery("Transactions.findByCheckoutDate", Transactions.class);
        query.setParameter("checkoutDate", checkoutDate);
        return query.getResultList();
    }

    public List<Transactions> findTransactionsByReturnDate(Date returnDate) {
        TypedQuery<Transactions> query = entityManager.createNamedQuery("Transactions.findByReturnDate", Transactions.class);
        query.setParameter("returnDate", returnDate);
        return query.getResultList();
    }

    // Add other methods as needed

}
