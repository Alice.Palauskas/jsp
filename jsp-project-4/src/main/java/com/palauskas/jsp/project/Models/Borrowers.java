/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.palauskas.jsp.project.Models;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author vboxuser
 */
@Entity
@Table(name = "Borrowers")
@NamedQueries({
    @NamedQuery(name = "Borrowers.findAll", query = "SELECT b FROM Borrowers b"),
    @NamedQuery(name = "Borrowers.findByBorrowerID", query = "SELECT b FROM Borrowers b WHERE b.borrowerID = :borrowerID"),
    @NamedQuery(name = "Borrowers.findByBorrowerName", query = "SELECT b FROM Borrowers b WHERE b.borrowerName = :borrowerName"),
    @NamedQuery(name = "Borrowers.findByContactNumber", query = "SELECT b FROM Borrowers b WHERE b.contactNumber = :contactNumber"),
    @NamedQuery(name = "Borrowers.findByEmail", query = "SELECT b FROM Borrowers b WHERE b.email = :email")})
public class Borrowers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "BorrowerID")
    private Integer borrowerID;
    @Basic(optional = false)
    @Column(name = "BorrowerName")
    private String borrowerName;
    @Column(name = "ContactNumber")
    private String contactNumber;
    @Column(name = "Email")
    private String email;
    @OneToMany(mappedBy = "borrowerID")
    private Collection<Transactions> transactionsCollection;

    public Borrowers() {
    }

    public Borrowers(Integer borrowerID) {
        this.borrowerID = borrowerID;
    }

    public Borrowers(Integer borrowerID, String borrowerName) {
        this.borrowerID = borrowerID;
        this.borrowerName = borrowerName;
    }

    public Integer getBorrowerID() {
        return borrowerID;
    }

    public void setBorrowerID(Integer borrowerID) {
        this.borrowerID = borrowerID;
    }

    public String getBorrowerName() {
        return borrowerName;
    }

    public void setBorrowerName(String borrowerName) {
        this.borrowerName = borrowerName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Collection<Transactions> getTransactionsCollection() {
        return transactionsCollection;
    }

    public void setTransactionsCollection(Collection<Transactions> transactionsCollection) {
        this.transactionsCollection = transactionsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (borrowerID != null ? borrowerID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Borrowers)) {
            return false;
        }
        Borrowers other = (Borrowers) object;
        if ((this.borrowerID == null && other.borrowerID != null) || (this.borrowerID != null && !this.borrowerID.equals(other.borrowerID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.palauskas.jsp.project.Models.Borrowers[ borrowerID=" + borrowerID + " ]";
    }
    
}
