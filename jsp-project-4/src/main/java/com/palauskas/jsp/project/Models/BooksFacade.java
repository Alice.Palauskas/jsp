package com.palauskas.jsp.project.Models;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

@Stateless
public class BooksFacade {

    @PersistenceContext
    private EntityManager entityManager;

    public void createBook(Books book) {
        entityManager.persist(book);
    }

    public void updateBook(Books book) {
        entityManager.merge(book);
    }

    public void removeBook(String isbn) {
        Books book = findBookById(isbn);
        if (book != null) {
            entityManager.remove(book);
        }
    }

    public Books findBookById(String isbn) {
        return entityManager.find(Books.class, isbn);
    }

    public List<Books> findAllBooks() {
        TypedQuery<Books> query = entityManager.createNamedQuery("Books.findAll", Books.class);
        return query.getResultList();
    }

    public List<Books> findBooksByGenre(String genre) {
        TypedQuery<Books> query = entityManager.createNamedQuery("Books.findByGenre", Books.class);
        query.setParameter("genre", genre);
        return query.getResultList();
    }

    public List<Books> findBooksPublishedAfterDate(Date date) {
        TypedQuery<Books> query = entityManager.createNamedQuery("Books.findByPublishedDate", Books.class);
        query.setParameter("publishedDate", date);
        return query.getResultList();
    }

    public List<Books> findBooksByAuthor(Authors author) {
        TypedQuery<Books> query = entityManager.createNamedQuery("Books.findByAuthor", Books.class);
        query.setParameter("authorID", author);
        return query.getResultList();
    }

}
