/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.palauskas.jsp.project.Models;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author vboxuser
 */
@Entity
@Table(name = "Transactions")
@NamedQueries({
    @NamedQuery(name = "Transactions.findAll", query = "SELECT t FROM Transactions t"),
    @NamedQuery(name = "Transactions.findByTransactionID", query = "SELECT t FROM Transactions t WHERE t.transactionID = :transactionID"),
    @NamedQuery(name = "Transactions.findByCheckoutDate", query = "SELECT t FROM Transactions t WHERE t.checkoutDate = :checkoutDate"),
    @NamedQuery(name = "Transactions.findByReturnDate", query = "SELECT t FROM Transactions t WHERE t.returnDate = :returnDate")})
public class Transactions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "TransactionID")
    private Integer transactionID;
    @Column(name = "CheckoutDate")
    @Temporal(TemporalType.DATE)
    private Date checkoutDate;
    @Column(name = "ReturnDate")
    @Temporal(TemporalType.DATE)
    private Date returnDate;
    @JoinColumn(name = "ISBN", referencedColumnName = "ISBN")
    @ManyToOne
    private Books isbn;
    @JoinColumn(name = "BorrowerID", referencedColumnName = "BorrowerID")
    @ManyToOne
    private Borrowers borrowerID;

    public Transactions() {
    }

    public Transactions(Integer transactionID) {
        this.transactionID = transactionID;
    }

    public Integer getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(Integer transactionID) {
        this.transactionID = transactionID;
    }

    public Date getCheckoutDate() {
        return checkoutDate;
    }

    public void setCheckoutDate(Date checkoutDate) {
        this.checkoutDate = checkoutDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public Books getIsbn() {
        return isbn;
    }

    public void setIsbn(Books isbn) {
        this.isbn = isbn;
    }

    public Borrowers getBorrowerID() {
        return borrowerID;
    }

    public void setBorrowerID(Borrowers borrowerID) {
        this.borrowerID = borrowerID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (transactionID != null ? transactionID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transactions)) {
            return false;
        }
        Transactions other = (Transactions) object;
        if ((this.transactionID == null && other.transactionID != null) || (this.transactionID != null && !this.transactionID.equals(other.transactionID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.palauskas.jsp.project.Models.Transactions[ transactionID=" + transactionID + " ]";
    }
    
}
