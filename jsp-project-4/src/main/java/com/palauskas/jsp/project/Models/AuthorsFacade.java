package com.palauskas.jsp.project.Models;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import java.util.List;

@Stateless
public class AuthorsFacade {

    @PersistenceContext
    private EntityManager entityManager;

    public void createAuthor(Authors author) {
        entityManager.persist(author);
    }

    public void updateAuthor(Authors author) {
        entityManager.merge(author);
    }

    public void removeAuthor(Integer authorID) {
        Authors author = findAuthorById(authorID);
        if (author != null) {
            entityManager.remove(author);
        }
    }

    public Authors findAuthorById(Integer authorID) {
        return entityManager.find(Authors.class, authorID);
    }

    public List<Authors> findAllAuthors() {
        TypedQuery<Authors> query = entityManager.createNamedQuery("Authors.findAll", Authors.class);
        return query.getResultList();
    }

    public List<Authors> findAuthorsByName(String authorName) {
        TypedQuery<Authors> query = entityManager.createNamedQuery("Authors.findByAuthorName", Authors.class);
        query.setParameter("authorName", authorName);
        return query.getResultList();
    }

    // Add other methods as needed

}
