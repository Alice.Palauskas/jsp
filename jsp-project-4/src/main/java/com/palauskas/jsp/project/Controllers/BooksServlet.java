package com.palauskas.jsp.project.Controllers;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import com.palauskas.jsp.project.Models.*;
import jakarta.ejb.EJB;
import java.util.Date;
import java.time.*;

public class BooksServlet extends HttpServlet {
    
    @EJB
    private BooksFacade booksFacade;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String editing = (String)request.getParameter("edit");
        String bookIsbn = (String)request.getParameter("isbn");
        
        if (editing != null && editing.equals("true") && bookIsbn != null)
        {
            request.setAttribute("editing", editing);
            request.setAttribute("book", booksFacade.findBookById(bookIsbn));
        }
        else
        {
            request.setAttribute("books", booksFacade.findAllBooks());
        }
        
        request.getRequestDispatcher("/books.jsp")
                .forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        Books book = booksFacade.findBookById(request.getParameter("isbn"));
        book.setTitle(request.getParameter("title"));
        book.setAuthorID(new Authors(Integer.valueOf(request.getParameter("authorId"))));
        book.setGenre(request.getParameter("genre"));
        book.setQuantityAvailable(Integer.valueOf(request.getParameter("quantity")));
        book.setPublishedDate(Date.from(LocalDate.parse(request.getParameter("date")).atStartOfDay(ZoneId.systemDefault()).toInstant()));
        
        booksFacade.updateBook(book);
        
        response.sendRedirect(request.getContextPath() + "/books");
    }
}
