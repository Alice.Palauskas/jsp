package com.palauskas.jsp.project.Models;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import java.util.List;

@Stateless
public class BorrowersFacade {

    @PersistenceContext
    private EntityManager entityManager;

    public void createBorrower(Borrowers borrower) {
        entityManager.persist(borrower);
    }

    public void updateBorrower(Borrowers borrower) {
        entityManager.merge(borrower);
    }

    public void removeBorrower(Integer borrowerID) {
        Borrowers borrower = findBorrowerById(borrowerID);
        if (borrower != null) {
            entityManager.remove(borrower);
        }
    }

    public Borrowers findBorrowerById(Integer borrowerID) {
        return entityManager.find(Borrowers.class, borrowerID);
    }

    public List<Borrowers> findAllBorrowers() {
        TypedQuery<Borrowers> query = entityManager.createNamedQuery("Borrowers.findAll", Borrowers.class);
        return query.getResultList();
    }

    public List<Borrowers> findBorrowersByName(String borrowerName) {
        TypedQuery<Borrowers> query = entityManager.createNamedQuery("Borrowers.findByBorrowerName", Borrowers.class);
        query.setParameter("borrowerName", borrowerName);
        return query.getResultList();
    }

    public List<Borrowers> findBorrowersByContactNumber(String contactNumber) {
        TypedQuery<Borrowers> query = entityManager.createNamedQuery("Borrowers.findByContactNumber", Borrowers.class);
        query.setParameter("contactNumber", contactNumber);
        return query.getResultList();
    }

    public List<Borrowers> findBorrowersByEmail(String email) {
        TypedQuery<Borrowers> query = entityManager.createNamedQuery("Borrowers.findByEmail", Borrowers.class);
        query.setParameter("email", email);
        return query.getResultList();
    }

    // Add other methods as needed

}
