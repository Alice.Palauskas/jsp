<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Книги - Районная библиотека</title>
        <link rel="stylesheet" href="books.css"/>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/header.jspf" %>
        <main>
            <h2>Книги</h2>
            <%
                String editing = (String)request.getAttribute("editing");
                if (editing != null && editing.equals("true")) { %>
                <form action="books" method="post">
                    <p>ISBN: <input type="text" name="isbn" readonly value="${book.getIsbn()}" /></p>
                    <p>Название: <input type="text" name="title" value="${book.getTitle()}" /></p>
                    <p>Автор: <input type="text" name="authorId" value="${book.getAuthorID().getAuthorID()}" /></p>
                    <p>Жанр: <input type="text" name="genre" value="${book.getGenre()}" /></p>
                    <p>Дата публикации: <input type="text" name="date" value="${book.getPublishedDateString()}" /></p>
                    <p>Доступное количество: <input type="text" name="quantity" value="${book.getQuantityAvailable()}" /></p>
                <button type="submit">Сохранить</button>
                </form>
            <% } else { %>
                <table>
                    <thead>
                        <tr>
                            <th>ISBN</th>
                            <th>Название</th>
                            <th>Автор</th>
                            <th>Жанр</th>
                            <th>Дата публикации</th>
                            <th>Количество</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="book" items="${books}">
                            <tr>
                                <td>${book.getIsbn()}</td>
                                <td>${book.getTitle()}</td>
                                <td>${book.getAuthorID().getAuthorName()}</td>
                                <td>${book.getGenre()}</td>
                                <td>${book.getPublishedDateString()}</td>
                                <td>${book.getQuantityAvailable()}</td>
                                <td><a href="?edit=true&isbn=${book.getIsbn()}">Редактировать</a></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            <% } %>
        </main>
        <%@include file="WEB-INF/jspf/footer.jspf" %>
    </body>
</html>
