<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Районная библиотека</title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/header.jspf" %>
        <main>
            <p>Добро пожаловать на сайт районной библиотеки!</p>
            <a href="books.jsp">Книги</a>
            <a href="borrowers.jsp">Посетители</a>
            <a href="transactions.jsp">Транзакции</a>
        </main>
        <%@include file="WEB-INF/jspf/footer.jspf" %>
    </body>
</html>
