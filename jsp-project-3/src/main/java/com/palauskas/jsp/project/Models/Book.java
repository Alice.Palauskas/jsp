package com.palauskas.jsp.project.Models;

import java.time.LocalDate;

public class Book {
    private String ISBN;
    private String Title;
    private int AuthorId;
    private String Genre;
    private LocalDate PublishedDate;
    private int QuantityAvailable;
    
    public String getISBN()
    {
        return ISBN;
    }
    
    public void setISBN(String isbn)
    {
        ISBN = isbn;
    }
    
    public String getTitle()
    {
        return Title;
    }
    
    public void setTitle(String title)
    {
        Title = title;
    }
    
    public String getGenre()
    {
        return Genre;
    }
    
    public void setGenre(String genre)
    {
        Genre = genre;
    }
    
    public int getAuthorId()
    {
        return AuthorId;
    }
    
    public void setAuthorId(int authorId)
    {
        AuthorId = authorId;
    }
    
    public int getQuantityAvailable()
    {
        return QuantityAvailable;
    }
    
    public void setQuantityAvailable(int quantityAvailable)
    {
        QuantityAvailable = quantityAvailable;
    }
    
    public LocalDate getPublishedDate()
    {
        return PublishedDate;
    }
    
    public void setPublishedDate(LocalDate publishedDate)
    {
        PublishedDate = publishedDate;
    }
}
