package com.palauskas.jsp.project.Controllers;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.ServletConfig;
import com.palauskas.jsp.project.Models.*;
import java.time.LocalDate;

public class BooksServlet extends HttpServlet {
    private static Book book = new Book();
    
    @Override
    public void init(ServletConfig config) throws ServletException
    {
        super.init();
        
        book = new Book();
        book.setISBN("123");
        book.setTitle("title");
        book.setAuthorId(1);
        book.setGenre("genre");
        book.setQuantityAvailable(10);
        book.setPublishedDate(LocalDate.now());
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("book", book);
        String test = (String)request.getParameter("edit");
        request.setAttribute("editing", test);
        request.getRequestDispatcher("/books.jsp")
                .forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        book.setISBN(request.getParameter("isbn"));
        book.setTitle(request.getParameter("title"));
        book.setAuthorId(Integer.parseInt(request.getParameter("authorId")));
        book.setGenre(request.getParameter("genre"));
        book.setQuantityAvailable(Integer.parseInt(request.getParameter("quantity")));
        book.setPublishedDate(LocalDate.parse(request.getParameter("date")));
        
        response.sendRedirect(request.getContextPath() + "/books");
    }
}
