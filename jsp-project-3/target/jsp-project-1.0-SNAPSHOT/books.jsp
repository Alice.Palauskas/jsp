<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Книги - Районная библиотека</title>
    </head>
    <body>
        <%@include file="WEB-INF/jspf/header.jspf" %>
        <main>
            <h2>Книги</h2>
            <%
                String editing = (String)request.getAttribute("editing");
                if (editing != null && editing.equals("true")) { %>
                <form action="books" method="post">
                    <p>ISBN: <input type="text" name="isbn" value="${book.getISBN()}" /></p>
                    <p>Название: <input type="text" name="title" value="${book.getTitle()}" /></p>
                    <p>Автор: <input type="text" name="authorId" value="${book.getAuthorId()}" /></p>
                    <p>Жанр: <input type="text" name="genre" value="${book.getGenre()}" /></p>
                    <p>Дата публикации: <input type="text" name="date" value="${book.getPublishedDate()}" /></p>
                    <p>Доступное количество: <input type="text" name="quantity" value="${book.getQuantityAvailable()}" /></p>
                <button type="submit">Сохранить</button>
                </form>
            <% } else { %>
                <p>ISBN: ${book.getISBN()}</p>
                <p>Название: ${book.getTitle()}</p>
                <p>Автор: ${book.getAuthorId()}</p>
                <p>Жанр: ${book.getGenre()}</p>
                <p>Дата публикации: ${book.getPublishedDate()}</p>
                <p>Доступное количество: ${book.getQuantityAvailable()}</p>
                <a href="?edit=true">Редактировать</a>
            <% } %>
        </main>
        <%@include file="WEB-INF/jspf/footer.jspf" %>
    </body>
</html>
