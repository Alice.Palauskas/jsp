<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Посетители - Районная библиотека</title>
    </head>
    <body>
        <header><h1>Районная библиотека</h1></header>
        <main>Посетители</main>
        <footer>&copy; Районная библиотека, 2023</footer>
    </body>
</html>
